# brown_rabbit
HTML
CSS
Bootstrap
Jquery
Photoshop

I used bootstrap css and js library for responsive website in whole devices. I make my own structure and use bootstrap css and also my own custom css according to my need.

I used bootstrap grid system in whole website for divide the page in different size's divs. I used jquery for random image banner. 

I used fontawesome library for social icons. For shadow and gradient effects i used css and background picture property both.

I completed The pagination with jquery. I put click event on read more button for show the rest of the text with bootstrap collapse component.


